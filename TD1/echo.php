    <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <ul>
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */

          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          /*$texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;

        $prenom = "Marc";

        echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";

        $utilisateur = [
            'prenom' => 'Juste',
            'nom'    => 'Leblanc'
        ];
        $utilisateur['passion'] = 'maquettes en allumettes';
        $utilisateur[] = "Nouvelle valeur";


        foreach ($utilisateur as $cle => $valeur){
            echo "$cle : $valeur\n";}*/

         $nom = "nedjar";
         $prenom = "younes";
         $login = "nedjary";
        echo "Utilisateur $prenom $nom de login $login\n";

        $utilisateur1 = [
                'nom' => 'nedjar',
                'prenom' => 'younes',
                'login' => 'nedjary'
        ];
        $utilisateur2 = [
            'nom' => 'nedjar',
            'prenom' => 'younes',
            'login' => 'nedjary'
        ];
        $utilisateur3 = [
            'nom' => 'nedjar',
            'prenom' => 'younes',
            'login' => 'nedjary'
        ];

        echo "\nUtilisateur $utilisateur1[nom]";

        $liste[] = $utilisateur1;
        $liste[] = $utilisateur2;
        $liste[] = $utilisateur3;

        echo "\nListe des utilisateurs :\n";

        if (empty($liste)){
            echo "Il n’y a aucun utilisateur.";
        }
        else {
            foreach ($liste as $cle => $valeur) {
                echo "<li> Utilisateur {$valeur['nom']} {$valeur['prenom']} {$valeur['login']}</li>\n";
            }
        }

        ?>
        </ul>
    </body>
</html> 